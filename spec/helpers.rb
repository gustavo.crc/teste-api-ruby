module Helpers
  def get_fixture(fixture)
    # symbolize_names: true serve para transformar os campos do yml em simbolo, para auxiliar no manuseio dos dados
    YAML.load(File.read(Dir.pwd + "/spec/cenarios/fixtures/#{fixture}.yaml"), symbolize_names: true)
  end

  module_function :get_fixture
end