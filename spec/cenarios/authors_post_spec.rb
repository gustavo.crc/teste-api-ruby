describe 'POST /Authors' do
  context 'Cadastrar um Novo Autor com Sucesso' do
    before(:all) do
      # Criacao de um payload para envio do corpo da mensagem do POST
      payload = {
        id: 1001,
        idBook: 410,
        firstName: 'Gustavo',
        lastName: 'Carranca'
      }

      @response = Authors.new.post_author(payload)

      # Tranformando a variavel de retorno em um hash, para facilitar o trabalho de validacao
      @response_parsed = @response.parsed_response
    end

    it 'Validando StatusCode 200' do
      expect(@response.code).to eql 200
    end

    it 'Validando o ID Cadastrado no Payload' do
      # Validando o ID do autor cadastrado se e exatamente retornado no response.body
      expect(@response_parsed['id']).to eql 1001
    end

    it 'Validando o IDBook Cadastrado no Payload' do
      # Validando o IDBook do autor cadastrado se e exatamente retornado no response.body
      expect(@response_parsed['idBook']).to eql 410
    end

    it 'Validando o FirstName Cadastrado no Payload' do
      # Validando o FirstName do autor cadastrado se e exatamente retornado no response.body
      expect(@response_parsed['firstName']).to eql 'Gustavo'
    end

    it 'Validando o LastName Cadastrado no Payload' do
      # Validando o LastName do autor cadastrado se e exatamente retornado no response.body
      expect(@response_parsed['lastName']).to eql 'Carranca'
    end
  end

  examples = Helpers.get_fixture('authors_POST')

  # each é parecido com o loop, ele irá varrer toda a lista do arquivo de massa de dados e pegar os campos de cada elemento
  examples.each do |e|
    context e[:titulo] do
      before(:all) do
        @response = Authors.new.post_author(e[:payload])
      end

      it 'Validando o StatusCode da Resposta' do
        expect(@response.code).to eql e[:code]
      end

      it 'Validando a Mensagem de Erro da Resposta' do
        expect(@response.parsed_response).to include(e[:error])
      end
    end
  end
end
