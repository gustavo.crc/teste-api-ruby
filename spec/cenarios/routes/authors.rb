require_relative './base_api.rb'

class Authors < BaseApi
  def post_author(payload)
    return self.class.post(
      '/api/v1/Authors',
      body: payload.to_json,
      headers: {
        'Content-Type': 'application/json'
      }
    )
  end

  def get_author()
    return self.class.get(
      '/api/v1/Authors',
      headers: {
        'Content-Type': 'application/json'
      }
    )
  end
end