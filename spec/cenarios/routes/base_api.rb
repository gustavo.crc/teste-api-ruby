require 'httparty'

class BaseApi
  # Importando o httparty para o class
  include HTTParty

  base_uri 'https://fakerestapi.azurewebsites.net'
end