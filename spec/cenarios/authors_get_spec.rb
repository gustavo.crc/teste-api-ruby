require 'httparty'

describe 'GET /Authors' do
  context 'Consultar da Lista de Autores com Sucesso' do
    before(:all) do
      @response = Authors.new.get_author
    end

    it 'Validando StatusCode 200' do
      expect(@response.code).to eql 200
    end

    it 'Validando se o Tamanho da Lista de Autores é Maior que 0' do
      # Tranformando a variavel de retorno em um hash, para facilitar o trabalho de validacao
      response_parsed = @response.parsed_response

      expect(response_parsed.size).to be > 0
    end
  end
end